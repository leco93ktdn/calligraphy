<?php
session_start();
include('partials/connect.php');

//Genrating CSRF Token
if (empty($_SESSION['token'])) :
    $_SESSION['token'] = bin2hex(random_bytes(32));
endif;

if (!isset($_GET['nid'])) :
    echo "<script type='text/javascript'> document.location = './index.php'; </script>";
endif;

if (isset($_POST['submit'])) :
    //Verifying CSRF Token
    if (!empty($_POST['csrftoken'])) :
        if (hash_equals($_SESSION['token'], $_POST['csrftoken'])) :
            $name = $_POST['name'];
            $email = $_POST['email'];
            $comment = $_POST['comment'];
            $postid = $_GET['nid'];
            $st1 = '0';
            if (isset($conn)) {
                $query = "insert into feedback(postId,name,email,comment,status) values('$postid','$name','$email','$comment','$st1')";
                $result = mysqli_query($conn, $query);
            }
            if ($result):
                echo
                "<script>alert('comment successfully submit. Comment will be display after admin review ');</script>";
                $_SESSION['token'] = bin2hex(random_bytes(32));
            else :
                echo "<script>alert('Something went wrong. Please try again.');</script>";
            endif;
        endif;
    endif;
endif;
?>
<!DOCTYPE HTML>
<html>
<?php
include("partials/head.php");
?>
<body>
<!-- header -->
<?php
include("partials/header.php");
?>
<!-- header -->
<!-- Blog -->
<div class="container">
    <div class="blog">

        <?php
        $pid = $_GET['nid'];
        $query = "select  p.postTitle, p.postImage, c.categoryName, c.id as cid,
                                                    sc.subcategory, p.postDetails, p.postingDate, p.postUrl
                                            from post as p 
                                                    left join category as c on c.id = p.CategoryId
                                                    left join subcategory as sc on sc.SubCategoryId = p.SubCategoryId
                                            where p.id ='$pid'";
        $result = mysqli_query($conn, $query);
        while ($row = mysqli_fetch_array($result)) :
            ?>
            <div class="blog-content">
                <div class="blog-content-left">
                    <div class="blog-artical-info">
                        <div class="blog-artical-info-img">
                            <div class="blog-content-img"
                                 style="background-image: url('admin/postimages/<?php echo htmlentities($row['postImage']); ?>')">
                            </div>
                        </div>
                        <div class="blog-artical-info-head">
                            <h2><a href="#"><?php echo htmlentities($row['postTitle']); ?></a></h2>
                            <h6><?php
                                $pt = $row['postDetails'];
                                echo(substr($pt, 0)); ?></h6>
                        </div>
                        <div class="artical-links">
                            <?php
                            $sts = 1;
                            $query = "select name, comment, postingDate from  feedback 
                                        where postId='$pid' and status='$sts'
                                        order by postingDate desc";
                            $result = mysqli_query($conn, $query);
                            while ($row = mysqli_fetch_array($result)) :
                                ?>
                                <ul>
                                    <li>
                                        <span><?php echo htmlentities($row['postingDate']); ?></span>
                                    </li>

                                    <li>
                                        <small class="admin"></small>
                                        <span><?php echo htmlentities($row['name']); ?></span>
                                    </li>
                                    <li>
                                        <small class="no"> </small>
                                        <span> <?php echo htmlentities($row['comment']); ?> </span>
                                    </li>
                                </ul><br/>
                            <?php endwhile; ?>
                        </div>
                        <div class="respon">
                            <div class='comment'>
                                <?php if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true): ?>
                                    <h2>Leave a comment</h2>
                                    <form name='Comment' method='post'>
                                        <input type="hidden" name="csrftoken" value="<?= $_SESSION['token'] ?>">
                                        <input type="text" name="name" class="textbox" value="Name"
                                               onfocus="this.value = '';"
                                               onblur="if (this.value == '') {this.value = 'Name';}">
                                        <input type="text" name="email" class="textbox" value="Email"
                                               onfocus="this.value = '';"
                                               onblur="if (this.value == '') {this.value = 'Email';}">
                                        <textarea value="Message:" name="comment" onfocus="this.value = '';"
                                                  onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
                                        <div class='smt1'>
                                            <input type='submit' name='submit' value='add a comment'>
                                        </div>
                                    </form>
                                <?php else: ?>
                                    <h2>Login to comment </h2>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php endwhile; ?>

        <?php include("partials/sidebar.php"); ?>

        <?php include("partials/footer.php"); ?>
    </div>
</div>
</body>
</html>