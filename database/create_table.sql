-- drop database finalproject;

CREATE DATABASE finalproject CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE finalproject;


CREATE TABLE accounts
(
    id         int AUTO_INCREMENT PRIMARY KEY,
    username   varchar(50)  NOT NULL,
    email      varchar(50)  NOT NULL,
    password   varchar(255) NOT NULL,
    address    text         NOT NULL,
    Is_Active  int(11)      NOT NULL,
    created_at datetime DEFAULT current_timestamp()
);


CREATE TABLE contacts
(
    id         int PRIMARY KEY AUTO_INCREMENT,
    name       varchar(255) NOT NULL,
    email      varchar(255) NOT NULL,
    subject    varchar(255) NOT NULL,
    message    text         NOT NULL,
    created_at datetime DEFAULT current_timestamp()
);


CREATE TABLE pages
(
    id           int PRIMARY KEY AUTO_INCREMENT,
    PageName     varchar(255) NOT NULL,
    PageTitle    varchar(255) NOT NULL,
    Description  longtext     NOT NULL,
    PostingDate  timestamp    NOT NULL DEFAULT current_timestamp(),
    UpdationDate timestamp    NOT NULL DEFAULT current_timestamp()
);


CREATE TABLE category
(
    id           int PRIMARY KEY AUTO_INCREMENT,
    CategoryName varchar(255) NOT NULL,
    Description  text         NOT NULL,
    PostingDate  timestamp    NOT NULL DEFAULT current_timestamp(),
    UpdationDate timestamp    NOT NULL DEFAULT current_timestamp(),
    Is_Active    int(1)       NOT NULL
);


CREATE TABLE subcategory
(
    SubCategoryId     int PRIMARY KEY AUTO_INCREMENT,
    CategoryId        int          NOT NULL,
    Subcategory       varchar(255) NOT NULL,
    SubCatDescription mediumtext   NOT NULL,
    PostingDate       timestamp    NOT NULL DEFAULT current_timestamp(),
    UpdationDate      timestamp    NOT NULL DEFAULT current_timestamp(),
    Is_Active         int(1)       NOT NULL,
    constraint fk_subcategory_category foreign key (CategoryId) REFERENCES category (id)
);


CREATE TABLE post
(
    id            int PRIMARY KEY AUTO_INCREMENT,
    PostTitle     longtext     NOT NULL,
    CategoryId    int(11)      NOT NULL,
    SubCategoryId int(11)      NOT NULL,
    PostDetails   longtext     NOT NULL,
    PostingDate   timestamp    NOT NULL DEFAULT current_timestamp(),
    UpdationDate  timestamp    NOT NULL DEFAULT current_timestamp(),
    Is_Active     int(1)       NOT NULL,
    PostUrl       mediumtext   NOT NULL,
    PostImage     varchar(255) NOT NULL,
    constraint fk_post_category foreign key (CategoryId) REFERENCES category (id),
    constraint fk_post_subcategory foreign key (SubCategoryId) REFERENCES subcategory (SubCategoryId)

);


CREATE TABLE feedback
(
    id          int PRIMARY KEY AUTO_INCREMENT,
    postId      int,
    name        varchar(120) NOT NULL,
    email       varchar(120) NOT NULL,
    comment     mediumtext   NOT NULL,
    postingDate timestamp    NOT NULL DEFAULT current_timestamp(),
    status      int(1)       NOT NULL,
    constraint fk_feedback_post foreign key (postId) REFERENCES post (id)
);