
<header class="header">
    <nav class="nav">
        <a href="index.php"><img class="nav__logo" src="./images/calligraphy.png" alt="logo" /></a>
        <ul class="nav__links">
            <li class="nav__item"><a href="index.php"" class="nav__link">Home</a></li>
            <li class="nav__item"><a href="about.php" class="nav__link">About Us</a></li>
            <li class="nav__item"><a href="gallery.php" class="nav__link">Gallery</a></li>
            <li class="nav__item"><a href="contact.php" class="nav__link">Contact</a></li>
            <?php
            if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true):
                echo'<li class="nav__item">
                <a href="logout.php" class="nav__link nav__link--btn btn--show-modal " style="background-color:#f53f1a;color: white"
                >LogOut</a
                >
            </li>';
            else:
                echo'<li class="nav__item">
                <a href="login.php" class="nav__link nav__link--btn btn--show-modal"
                >Login</a
                >
            </li>';
            endif;
            ?>

        </ul>
    </nav>
</header>
<script>
    const nav = document.querySelector('.nav')

    const handleHover = function (e) {
        if (e.target.classList.contains('nav__link')) {
            const link = e.target
            const siblings = link.closest('.nav').querySelectorAll('.nav__link')
            const logo = link.closest('.nav').querySelector('img')

            siblings.forEach((el) => {
                if (el !== link) el.style.opacity = this
            })
            logo.style.opacity = this
        }
    }

    nav.addEventListener('mouseover', handleHover.bind(0.5))
    nav.addEventListener('mouseout', handleHover.bind(1))

</script>
<!-- header -->
<!--<div class="header">-->
<!--		<div class="container">-->
<!--			<div class="logo">-->
<!--				<a href="index.php"><img src="images/Calligraphy.png" class="img-responsive" alt=""></a>-->
<!--			</div>-->
<!--			-->
<!--				<div class="head-nav">-->
<!--					<span class="menu"> </span>-->
<!--						<ul class="cl-effect-1">-->
<!--							<li class="active"><a href="index.php">Home</a></li>-->
<!--							<li><a href="about.php">About Us</a></li>-->
<!--							<li><a href="gallery.php">Gallery</a></li>							-->
<!--							<li><a href="contact.php">Contact</a></li>						-->
<!--							<li><a href="login.php">Login</a></li>							-->
<!--										<div class="clearfix"></div>-->
<!--						</ul>-->
<!--				</div>-->
	<!-- script-for-nav -->
<!--							<script>-->
<!--								$( "span.menu" ).click(function() {-->
<!--								  $( ".head-nav ul" ).slideToggle(300, function() {-->
<!--									// Animation complete.-->
<!--								  });-->
<!--								});-->
<!--							</script>-->
						<!-- script-for-nav -->
<!--						<div class="user">-->
<!--					<img src="../web/images/co.png" alt="" class="user__img">-->
<!--					<div class="user__action user__active">-->
<!--					<h5>Hi, --><?php //echo htmlspecialchars($_SESSION["username"]); ?><!--</h5>-->
<!--					<a href="confirm_pass.php"><i class="ti-settings m-r-5"></i> Change Password</a><br>                         -->
<!--						   <a href="logout.php"><i class="ti-power-off m-r-5"></i> Logout</a>-->
<!--						-->
<!---->
<!--					</div>-->
<!--				</div>			-->
<!--					<div class="clearfix"> </div>-->
<!--		</div>-->
<!--	</div>-->
<!-- header -->