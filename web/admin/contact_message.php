<?php
session_start();
include('includes/config.php');
error_reporting(0);
if ($_SESSION['username'] !== 'admin') {
    header('location:../index.php');
} else {
// Code for deletion
    if ($_GET['action'] == 'del' && $_GET['rid']) {
        $id = intval($_GET['rid']);
        if (isset($conn)) {
            $query = mysqli_query($conn, "delete from  contacts  where id='$id'");
        }
        $delmsg = "Message deleted forever";
    }

    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>

        <title> | Manage Contact Message</title>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/core.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../plugins/switchery/switchery.min.css">
        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body class="fixed-left">

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Top Bar Start -->
        <?php include('includes/topheader.php'); ?>

        <!-- ========== Left Sidebar Start ========== -->
        <?php include('includes/leftsidebar.php'); ?>
        <!-- Left Sidebar End -->


        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">


                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Manage Contact Messages</h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="#">Admin</a>
                                    </li>
                                    <li>
                                        <a href="#">Comments </a>
                                    </li>
                                    <li class="active">
                                        Messages
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->


                    <div class="row">
                        <div class="col-sm-6">

                            <?php if ($delmsg) { ?>
                                <div class="alert alert-danger" role="alert">
                                    <strong><?php echo htmlentities($delmsg); ?></strong></div>
                            <?php } ?>


                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="demo-box m-t-20">

                                    <div class="table-responsive">
                                        <table class="table m-0 table-colored-bordered table-bordered-primary">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th> Name</th>
                                                <th>Email</th>
                                                <th>Subject</th>
                                                <th width="300">Message</th>
                                                <th>Created Date</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $query = mysqli_query($conn, "Select * from contacts");
                                            $cnt = 1;
                                            while ($row = mysqli_fetch_array($query)) {
                                                ?>

                                                <tr>
                                                    <th scope="row"><?php echo htmlentities($cnt); ?></th>
                                                    <td><?php echo htmlentities($row['name']); ?></td>
                                                    <td><?php echo htmlentities($row['email']); ?></td>
                                                    <td><?php echo htmlentities($row['subject']); ?></td>
                                                    <td><?php echo($row['message']); ?></td>
                                                    <td><?php echo htmlentities($row['created_at']); ?></td>
                                                    <td>
                                                        &nbsp;<a
                                                                href="contact_message.php?rid=<?php echo htmlentities($row['id']); ?>&&action=del">
                                                            <i class="fa fa-trash-o" style="color: #f05050"></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                                $cnt++;
                                            } ?>
                                            </tbody>

                                        </table>
                                    </div>


                                </div>

                            </div>


                        </div>
                        <!--- end row -->


                        <div class="row">
                            <div class="col-md-12">
                                <div class="demo-box m-t-20">
                                    <div class="m-b-30">


                                    </div>


                                </div>

                            </div>


                        </div>


                    </div> <!-- container -->

                </div> <!-- content -->
                <?php include('includes/footer.php'); ?>
            </div>

        </div>
        <!-- END wrapper -->


        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="../plugins/switchery/switchery.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
    </html>
<?php } ?>