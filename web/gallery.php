<?php 
session_start();
include('partials/connect.php');
?>
<!DOCTYPE HTML>
<html>
<?php
    include ("partials/head.php");
?>
<body>
<?php
include ("partials/header.php");
?>
    <div class="container">
        <div class="col-md-9 bann-right">

<!-- header -->
            <div class="container">
                <div class="about">
                    <div class="team_grid">
                        <h3 class="m_1">Gallery</h3>
                        <div class="span_3">
<?php
if (isset($conn)) {
    $query=mysqli_query($conn,"select PostImage from post");
}
    while ($row=mysqli_fetch_array($query)) :
?>
                            <div class="col-md-4 ab-top" style="margin-bottom:30px"
                                 >
                                    <div class="gallery-content-img"
                                         style="background-image: url('./admin/postimages/<?php echo htmlentities($row['PostImage']);?>')"
                                    >

                                    </div>
                            </div>
<?php endwhile; ?>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>
<?php
    include ("partials/footer.php");
?>
</body>
</html>